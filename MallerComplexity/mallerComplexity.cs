﻿using Autodesk.Revit.ApplicationServices;
using Autodesk.Revit.Attributes;
using Autodesk.Revit.DB;
using Autodesk.Revit.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MallerComplexity
{
    public class FamilyData
    {
        public FamilyData(string category, string familyName, int score, int scoreNested)
        {
            FamilyName = familyName;
            Category = category;
            Score = score;
            ScoreNested = scoreNested;
        }
        public string FamilyName { get; set; }
        public string Category { get; set; }
        public int Score { get; set; }
        public int ScoreNested { get; set; }
    }

    [Transaction(TransactionMode.Manual)]
    public class computeMaller : IExternalCommand
    {
        static AddInId appId = new AddInId(new Guid("B32711B3-771A-4207-8C1B-891789513353"));
        public Result Execute(ExternalCommandData commandData, ref string message, ElementSet elementSet)
        {
            Application app = commandData.Application.Application;
            UIDocument uidoc = commandData.Application.ActiveUIDocument;

            System.Windows.Forms.FolderBrowserDialog folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            folderBrowserDialog.ShowNewFolderButton = false;
            folderBrowserDialog.SelectedPath = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
            if (folderBrowserDialog.ShowDialog() == System.Windows.Forms.DialogResult.Cancel)
                return Result.Cancelled;
            string folder = folderBrowserDialog.SelectedPath;

            List<FamilyData> data = new List<FamilyData>();

            foreach (string filename in Directory.GetFiles(folder, "*.rfa", SearchOption.AllDirectories))
            {
                if (isBackupFile(filename))
                    continue;

                Document doc = app.OpenDocumentFile(filename);
                int score = getScore(doc);

                int scoreNested = 0;
                foreach (Family nestedFam in new FilteredElementCollector(doc).OfClass(typeof(Family)))
                {
                    if (!nestedFam.IsEditable)
                        continue;

                    Document docNested = doc.EditFamily(nestedFam);
                    scoreNested += getScore(docNested);
                    docNested.Close(false);
                }

                data.Add(new FamilyData(doc.OwnerFamily.FamilyCategory.Name, Path.GetFileNameWithoutExtension(filename), score, scoreNested));

                doc.Close(false);
            }

            #region CSV export
            string csv = Path.Combine(folder, "data.csv");
            using (StreamWriter sw = new StreamWriter(csv, false))
            {
                sw.WriteLine(",Username,Category,Family Name,Score,Nested Family Score");
                foreach (FamilyData d in data)
                {
                    sw.WriteLine("Data courtesy of Boost Your BIM," + app.Username + "," + d.Category + "," + d.FamilyName + "," + d.Score + "," + d.ScoreNested);
                }
            }
            Process.Start(csv);
            #endregion

            #region Graph in Revit using walls and lines
            if (uidoc != null)
            {
                Transaction t = null;
                try
                {
                    Document resultsDoc = uidoc.Document;
                    using (t = new Transaction(resultsDoc, "graph"))
                    {
                        t.Start();
                        ViewPlan view = new FilteredElementCollector(resultsDoc).OfClass(typeof(ViewPlan)).Cast<ViewPlan>().FirstOrDefault(q => !q.IsTemplate);
                        Level level = new FilteredElementCollector(resultsDoc).OfClass(typeof(Level)).Cast<Level>().OrderBy(q => q.Elevation).FirstOrDefault();
                        int y = 0;
                        foreach (FamilyData fd in data)
                        {
                            Wall wall1 = Wall.Create(resultsDoc, Line.CreateBound(new XYZ(0, y, 0), new XYZ(fd.Score, y, 0)), level.Id, false);
                            wall1.get_Parameter(BuiltInParameter.ALL_MODEL_INSTANCE_COMMENTS).Set(Path.GetFileName(fd.FamilyName));
                            var score = wall1.Parameters.Cast<Parameter>().FirstOrDefault(q => q.Definition.Name == "Score");
                            if (score != null) score.Set(fd.Score);
                            var nested = wall1.Parameters.Cast<Parameter>().FirstOrDefault(q => q.Definition.Name == "NestedScore");
                            if (nested != null) nested.Set(fd.ScoreNested);
#if REVIT18PLUS
                            IndependentTag.Create(resultsDoc, resultsDoc.ActiveView.Id, new Reference(wall1), false, TagMode.TM_ADDBY_CATEGORY, TagOrientation.Horizontal, new XYZ(-1, y + 1.5, 0));
#else
                            resultsDoc.Create.NewTag(resultsDoc.ActiveView, wall1, false, TagMode.TM_ADDBY_CATEGORY, TagOrientation.Horizontal, new XYZ(-1, y + 1.5, 0));
#endif
                            if (fd.ScoreNested > 0)
                                makeLine(resultsDoc, new XYZ(fd.Score, y, 0), new XYZ(fd.Score + fd.ScoreNested, y, 0));

                            y += 10;
                        }
                        t.Commit();
                        t.Dispose();
                        t = null;
                    }
                }
                catch (Exception ex)
                {
                    TaskDialog td = new TaskDialog("Error in Maller Complexity Drawing");
                    td.MainContent = "An error occurred during the drawing of the graph with walls in the current model: " + ex.GetType().Name + ": " + ex.Message;
                    td.ExpandedContent = ex.StackTrace;
                    td.Show();
                    if ((t != null) && t.IsValidObject && (t.HasStarted())) t.RollBack();
                }
                
            }
#endregion

            return Result.Succeeded;
        }

        private bool isBackupFile(string filename)
        {
            return System.Text.RegularExpressions.Regex.IsMatch(filename, @".\d{4}.\w{3}\b", System.Text.RegularExpressions.RegexOptions.IgnoreCase);
        }


        public static ModelLine makeLine(Document doc, XYZ pt1, XYZ pt2)
        {
            if (pt1 == null || pt2 == null)
                return null;

            if (pt1.DistanceTo(pt2) < 0.01)
                return null;

            ModelLine ret = null;
            Transaction t = null;
            if (doc.IsModifiable == false)
            {
                t = new Transaction(doc, "g");
                t.Start();
            }
           
           
                ret = (ModelLine)doc.Create.NewModelCurve(Line.CreateBound(pt1, pt2), SketchPlane.Create(doc, makePlane(doc.Application, pt1, pt2)));

                if (t != null)
                    t.Commit();
            
            return ret;
        }

        private static Plane makePlane(Autodesk.Revit.ApplicationServices.Application app, XYZ pt1, XYZ pt2)
        {
            XYZ v = pt1.Subtract(pt2);
            double dxy = Math.Abs(v.X) + Math.Abs(v.Y);
            XYZ w = (dxy > 0.0001) ? XYZ.BasisZ : XYZ.BasisY;
            XYZ norm = v.CrossProduct(w).Normalize();
            XYZ v2 = norm.CrossProduct(w);
            XYZ v3 = norm.CrossProduct(v2);
#if REVIT18PLUS
            return Plane.Create(new Frame(pt1, norm, v2, v3));
#else
            return app.Create.NewPlane(norm, pt1);
#endif
        }

        private int getScore(Document doc)
        {
            FamilyManager fm = doc.FamilyManager;
            int paramCt = fm.Parameters.Size;
            int formulaChars = 0;

            List<char> operators = new List<char> { '>', '<', '+', '-', '*', '/' };

            foreach (FamilyParameter fp in fm.Parameters)
            {
                if (fp.Formula == null)
                    continue;

                foreach (char c in operators)
                {
                    formulaChars += fp.Formula.Count(q => q == c);
                }

            }
            int score = paramCt + formulaChars;
            return score;
        }
    }
}
